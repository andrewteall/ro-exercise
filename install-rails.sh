#!/bin/bash
#Bring the repos up to date and install dependencies
sudo apt-get update &&
sudo apt-get install -y ruby rubygems ruby-all-dev sqlite3 libsqlite3-dev &&
## Was running into dependency issues so just found them all
sudo apt-get install -y build-essential bison openssl libreadline6 libreadline6-dev curl git-core zlib1g zlib1g-dev libssl-dev libyaml-dev libxml2-dev autoconf libc6-dev ncurses-dev automake libtool nodejs &&
sudo gem install rails
#Create the app workspace
sudo mkdir -p /opt/rails
cd /opt/rails
sudo rails new blog
#Copy the service file over and start the app
sudo mv /home/ubuntu/rails-server.service /etc/systemd/system/
sudo chmod +x /etc/systemd/system/rails-server.service
#sudo systemctl enable rails-server
sudo systemctl daemon-reload
sudo systemctl start rails-server
