#!/bin/bash
export REGION=eu-west-1

################################################################################
# Functions
################################################################################
#getPulbicDNS - Prints public dns
getPulbicDNS () {
  local INSTANCE__ID=$1
  aws ec2 describe-instances --instance-ids $INSTANCE__ID --query 'Reservations[].Instances[].PublicDnsName' --region $REGION | sed -n 2p | tr -d ' ' | tr -d '"'
}

#createKeyPair - Creates a key pair of the specified name and saves it to ~/.ssh/$KEY_NAME.pem. Prints the Key Pair Name
createKeyPair () {
  local KEY_PAIR_NAME=$1
  aws ec2 create-key-pair --key-name $KEY_PAIR_NAME --region $REGION | grep KeyMaterial | cut -d ':' -f2 | tr -d ',' | sed 's/^\ //g' | sed 's/\ $//g' | tr -d '"' | sed 's/\\n/\n/g' > ~/.ssh/$KEY_PAIR_NAME.pem
  chmod 0400 ~/.ssh/$KEY_PAIR_NAME.pem
  echo $KEY_PAIR_NAME
}

#deleteKeyPair - Deletes the specified key pair by name
deleteKeyPair() {
  local KEY_PAIR_NAME=$1
  delete-key-pair --key-name $KEY_PAIR_NAME --region $REGION
}

#createVM - Prints instance id of the created VM
createVM () {
  local KEY_PAIR_NAME=$1
  local AMI_ID="ami-1d4cbc64" #eu-west-1	xenial	16.04 LTS	amd64	ebs-ssd	20170811
  if [[ -z KEY_PAIR_NAME ]]; then
    KEY_PAIR_NAME=andrew-teall2
  fi

  local INSTANCE_ID=$(aws ec2 run-instances --image-id $AMI_ID --count 1 --instance-type t1.micro --security-group-id sg-a4fd8adc --subnet-id subnet-b9a93ede --associate-public-ip-address --key-name $KEY_PAIR_NAME --region $REGION  | grep "InstanceId" | cut -d ':' -f2 | tr -d ',' | tr -d '"' | tr -d ' ' ) #--security-groups $my-sg
  echo $INSTANCE_ID
}

#installRails - Installs a default Rails server to the specified server with the supplied private key
installRails () {
  local SERVER_ADDRESS=$1
  local KEY_PAIR_NAME=$2
  scp -o IdentitiesOnly=yes -oStrictHostKeyChecking=no -i ~/.ssh/$(echo $KEY_PAIR_NAME) rails-server.service ubuntu@$SERVER_ADDRESS:rails-server.service
  ssh -o IdentitiesOnly=yes -oStrictHostKeyChecking=no -i ~/.ssh/$(echo $KEY_PAIR_NAME) ubuntu@$SERVER_ADDRESS "bash -s" < install-rails.sh
}

#terminateVM - Terminates a VM specified by the instance ID
terminateVM () {
  local INSTANCE_ID=$1
  aws ec2 terminate-instances --instance-ids $INSTANCE_ID --region $REGION
}

################################################################################
# Main Program
################################################################################
main () {
  KEY_PAIR_NAME=
  OUT=/dev/null
  while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
      -v|--verbose)
        export OUT=1
        ;;
      -k|--keyname)
        export KEY_PAIR_NAME=$2
        shift
        ;;
    esac
    shift
  done

  local KEY_PAIR_NAME_CREATED=false
  if [[ -z $KEY_PAIR_NAME ]]; then
    >&$OUT echo "No Key Pair Name supplied. One will be created for you"
    local KEY_PAIR_NAME=ROEX$RANDOM
    createKeyPair $KEY_PAIR_NAME
    local KEY_PAIR_NAME_CREATED=true
  fi

  local INSTANCE_ID=$(createVM $KEY_PAIR_NAME)
  >&$OUT echo "Use the InstanceId on the next line to perform actions on the created instance"
  >&$OUT echo $INSTANCE_ID
  #sleeping here because the DNS wasn't always available that quickly
  sleep 5
  local DNS_NAME=$(getPulbicDNS $INSTANCE_ID)
  #sleeping here to allow the vm to fully become active and to let the key pair propogate
  sleep 30
  installRails $DNS_NAME $KEY_PAIR_NAME.pem

  echo "================================================================================"
  echo "================================================================================"
  echo "Clink the link below to see the newly installed rails package"
  echo $DNS_NAME:3000
  echo
  #echo "To delete the running instance run:"
  #echo "terminateVM "$INSTANCE_ID
  if [[ $KEY_PAIR_NAME_CREATED = true ]]; then
    echo
    echo "A key pair has been created for you and was used to create all instances"
    echo "A private key from the key pair has been saved at ~/.ssh/$KEY_PAIR_NAME.pem"
    echo
  fi
  echo "To access the instance you may run:"
  echo "ssh -o IdentitiesOnly=yes -oStrictHostKeyChecking=no -i ~/.ssh/$KEY_PAIR_NAME.pem ubuntu@$DNS_NAME"
}

main "$@"
