#  ReactiveOps Exercise #

This repository contains the scripts and files need to complete the ReactiveOps Technical exercise.

-------------------------------------------------------------------------------

### Getting Started  ###
*Prerequisites:*

   *AWS CLI installed and authenticated against your account*

   *A default network allowing inbound traffic on port 22 and port 3000 from the ip address the script is run*

**Starting the Cluster**

To run the exercise, clone the repository and run:

    ./reactive-start.sh

At the completion of the script several parameters will print and a rails instance should be available at the printed address. During the course of the program a key pair will be generated and used to create the server, connect to it, and deploy the application. You can specify a key pair if you wish.

**Command Line Switches**

Two switches are available -v|--verbose and -k|--keyname

   -v - Gives a slightly more verbose output

   -k - Allows a specified keyname to be used to build and connect to the instance. Two assumptions are that the key pair name is the same as the pem file name and the key is located at ~/.ssh/$KEY_NAME.pem.
